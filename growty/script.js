'use strict'

document.addEventListener('click', documentClick);

function documentClick(event){
    const target = event.target;

    if(target.closest('.icon-menu')){
        document.documentElement.classList.toggle('menu-open');
    }
}