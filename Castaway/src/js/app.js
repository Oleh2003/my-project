"use strict";
// Burger menu ----------------------

const menu = document.querySelector(".header__menu-icon");

const menuNavbar = document.querySelector(".menu__navbar");

menu.addEventListener("click", () => {
  menu.classList.toggle("active");
  menuNavbar.classList.toggle("active");
});

// Rating ----------------------

const ratings = document.querySelectorAll(".rating-comment");

if (ratings.length > 0) {
  initRatings();
}
// Main function
function initRatings() {
  let ratingActive, ratingValue;
  for (let i = 0; i < ratings.length; i++) {
    const rating = ratings[i];
    initRating(rating);
  }
  // ініцілізація рейтингу
  function initRating(rating) {
    initRatingVars(rating);

    setRatingActiveWidth();

    if (rating.classList.contains("rating_set")) {
      setRating(rating);
    }
  }

  function initRatingVars(rating) {
    ratingActive = rating.querySelector(".rating-comment__active");
    ratingValue = rating.querySelector(".rating-comment__value");
  }

  function setRatingActiveWidth(index = ratingValue.innerHTML) {
    const ratingActiveWidth = index / 0.05;
    ratingActive.style.width = `${ratingActiveWidth}%`;
  }

  function setRating(rating) {
    const ratingItems = rating.querySelectorAll(".rating-comment__item");
    for (let i = 0; i < ratingItems.length; i++) {
      const ratingItem = ratingItems[i];
      ratingItem.addEventListener("mouseenter", function (e) {
        // Оновлення змінних
        initRatingVars(rating);
        // Обновлення активних зірок
        setRatingActiveWidth(ratingItem.value);
      });
      ratingItem.addEventListener("mouseleave", function (e) {
        setRatingActiveWidth();
      });
      ratingItem.addEventListener("click", function (e) {
        // Оновлення змінних
        initRatingVars(rating);

        ratingValue.innerHTML = index + 1;
        setRatingActiveWidth();
      });
    }
  }
}
