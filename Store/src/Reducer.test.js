import React from "react";
import { modalReducer } from "./redux/ModalReducer";
import { cartReducer } from "./redux/CartReducer";
import { favoritesReducer } from "./redux/FavoritesReducer";
import * as types from "./redux/type";


describe("cartReducer", () => {                              // CART REDUCER!
  it("should return the initial state", () => {
    const initialState = [];
    expect(cartReducer(undefined, {})).toEqual(initialState);
  });

  it("should handle LOAD_CART", () => {
    const initialState = [];
    const action = {
      type: types.LOAD_CART,
    };
    expect(cartReducer(initialState, action)).toEqual(initialState);
  });

  it("should handle ADD_CART", () => {
    const initialState = [];
    const action = {
      type: types.ADD_CART,
      data: { id: 1 },
    };
    const expectedState = [{ id: 1 }];
    expect(cartReducer(initialState, action)).toEqual(expectedState);
  });

  it("should handle SAVE_CART", () => {
    const initialState = [];
    const action = {
      type: types.SAVE_CART,
      data: [],
    };
    const expectedState = [];
    expect(cartReducer(initialState, action)).toEqual(expectedState);
  });

  it("should handle DELETE_CART", () => {
    const initialState = [];
    const action = {
      type: types.DELETE_CART,
      data: { id: [] },
    };
    const expectedState = [];
    expect(cartReducer(initialState, action)).toEqual(expectedState);
  });

  it("should handle SET_MODAL_FORM_DELETE_CART", () => {
    const initialState = [];
    const action = {
      type: types.SET_MODAL_FORM_DELETE_CART,
    };
    const expectedState = [];
    expect(cartReducer(initialState, action)).toEqual(expectedState);
  });
});

 
describe('favoritesReducer', () => {                                    // FAVORITES REDUCER!
    it('should return the initial state', () => {
      const initialState = [];
      expect(favoritesReducer(undefined, {})).toEqual(initialState);
    });
  
    it('should handle ADD_FAVORITES', () => {
      const initialState = [];
      const action = {
        type: types.ADD_FAVORITES,
        data: { id: 1 },
      };
      const expectedState = [{ id: 1 }];
      expect(favoritesReducer(initialState, action)).toEqual(expectedState);
    });
  
    it('should handle SAVE_FAVORITES', () => {
      const initialState = [];
      const action = {
        type: types.SAVE_FAVORITES,
        data: [],
      };
      const expectedState = [];
      expect(favoritesReducer(initialState, action)).toEqual(expectedState);
    });
  
    it('should handle LOAD_FAVORITES', () => {
      const initialState = [];
      const action = {
        type: types.LOAD_FAVORITES,
      };
      expect(favoritesReducer(initialState, action)).toEqual(initialState);
    });
  
    it('should handle DELETE_FAVORITES', () => {
      const initialState = [];
      const action = {
        type: types.DELETE_FAVORITES,
        data: { id: []},
      };
      const expectedState = [];
      expect(favoritesReducer(initialState, action)).toEqual(expectedState);
    });
  
  });



  describe('modalReducer', () => {                               // MODAL REDUCER
    it('should return the initial state', () => {
      const initialState = {
        isModalOpen: false,
        selectedItemId: null,
      };
      expect(modalReducer(undefined, {})).toEqual(initialState);
    });
  
    it('should handle SET_MODAL', () => {
      const initialState = {
        isModalOpen: false,
        selectedItemId: null,
      };
      const action = {
        type: types.SET_MODAL,
        data: {
          isOpen: true,
          id: [],
        },
      };
      const expectedState = {
        isModalOpen: true,
        selectedItemId: action.data.id,
      };
      expect(modalReducer(initialState, action)).toEqual(expectedState);
    });
  
    it('should handle SET_MODAL_FORM', () => {
      const initialState = {
        isModalOpen: false,
        selectedItemId: null,
      };
      const action = {
        type: types.SET_MODAL_FORM,
        data: {
          isOpenFrom: true,
          id: [],
        },
      };
      const expectedState = {
        ...initialState,
        isModalFormOpen: true,
        selectedItemIdFrom: action.data.id,
      };
      expect(modalReducer(initialState, action)).toEqual(expectedState);
    });
  
    it('should handle SET_MODAL_FORM_DELETE_CART', () => {
      const initialState = {
        isModalOpen: true,
        selectedItemId: null,
      };
      const action = {
        type: types.SET_MODAL_FORM_DELETE_CART,
      };
      const expectedState = {
        isModalOpen: false,
        selectedItemId: null,
      };
      expect(modalReducer(initialState, action)).toEqual(expectedState);
    });
  
  });
