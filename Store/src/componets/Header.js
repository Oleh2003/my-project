import React ,{useState} from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "../scss/header.scss";

function Header({ cart, favorites }) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };
  return (
    <header className="header">
      <div className={`header_menu-icon ${isMenuOpen ? "active" : ""}`} onClick={toggleMenu}>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <nav className={`header_navbar ${isMenuOpen ? "active" : ""}`}>
      <Link to="/" className="header_title">
        Home
      </Link>
      <Link to="/favorites" className="header_title">
        Favorites {favorites ? favorites.length : 0}
        
      </Link>
      <Link to="/cart" className="header_title">
        Cart {cart ? cart.length : 0}
        
      </Link>
  </nav>
  
        <button onClick={() => {}} className="header_btn">Get started</button>
      
    </header>
  );
}

Header.propTypes = {
  cart: PropTypes.arrayOf(PropTypes.object),
  favorites: PropTypes.arrayOf(PropTypes.object),
};

Header.defaultProps = {
  cart: [],
  favorites: [],
};

export default Header;
