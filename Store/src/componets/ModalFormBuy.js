import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { PatternFormat } from "react-number-format";
import "../scss/ModalFromBuy.scss";
import { useDispatch } from "react-redux";
import { deleteFromLocalStorageCart, setModalFormDeleteCart } from "../redux/action";

const validationSchema = Yup.object({
  name: Yup.string().required("Введіть ваше ім'я!"),
  lastName: Yup.string().required("Введіть ваше прізвище!"),
  age: Yup.string().required("Введіть ваш вік!").matches(/^[0-9]+$/, "Введіть ваш вік!"),
  address: Yup.string().required("Введіть вашу адресу!"),
  number: Yup.string().required("Введіть ваш номер!"),
});

function ModalFormBuy({
  isOpenForm,
  onRequestCloseFrom,
  closeButtonForm,
  card,
}) {
  const dispatch = useDispatch();

  const cartItems = JSON.parse(localStorage.getItem("cart"));
  const formik = useFormik({
    initialValues: {
      name: "",
      lastName: "",
      age: "",
      address: "",
      number: "",
      purchasedItems: [],
    },
    onSubmit: (values) => {
      console.log(values);
      dispatch(setModalFormDeleteCart(values));
      // console.log("Назва товару:", card.name);
      // console.log("Ціна товару:", card.price + " грн");
      // console.log("Код товару:", card.article);
      // console.log("Колір товару:", card.color);
      console.log("Придбані товари:", cartItems);
      dispatch(deleteFromLocalStorageCart(card))
      onRequestCloseFrom();
    },
    validationSchema,
  });

  // console.log(formik.errors);

  if (!isOpenForm) {
    return null;
  }

  return (
    <div className="modal-container">
      <form className="modal-form" onSubmit={formik.handleSubmit}>
        {closeButtonForm && (
          <button className="modal-form_close" onClick={onRequestCloseFrom}>
            &times;
          </button>
        )}
        <div className="form_control">
          <label className="modal-form_label" htmlFor="name">
            Ім'я користувача
          </label>
          <input
            className="modal-form_input"
            type="text"
            name="name"
            id="name"
            onChange={formik.handleChange}
            value={formik.values.name}
            onBlur={formik.handleBlur}
          />
          {formik.errors.name ? (
            <div className="error">
              {formik.touched.name && formik.errors.name}
            </div>
          ) : null}
        </div>
        <div className="form_control">
          <label className="modal-form_label" htmlFor="lastName">
            Прізвище користувача
          </label>
          <input
            className="modal-form_input"
            type="text"
            name="lastName"
            id="lastName"
            onChange={formik.handleChange}
            value={formik.values.lastName}
            onBlur={formik.handleBlur}
          />
          {formik.errors.lastName ? (
            <div className="error">
              {formik.touched.lastName && formik.errors.lastName}
            </div>
          ) : null}
        </div>

        <div className="form_control">
          <label className="modal-form_label" htmlFor="age">
            Вік користувача
          </label>
          <input
            className="modal-form_input"
            type="text"
            name="age"
            id="age"
            onChange={formik.handleChange}
            value={formik.values.age}
            onBlur={formik.handleBlur}
          />
          {formik.errors.age ? (
            <div className="error">
              {formik.touched.age && formik.errors.age}
            </div>
          ) : null}
        </div>

        <div className="form_control">
          <label className="modal-form_label" htmlFor="address">
            Адреса доставки
          </label>
          <input
            className="modal-form_input"
            type="text"
            name="address"
            id="address"
            onChange={formik.handleChange}
            value={formik.values.address}
            onBlur={formik.handleBlur}
          />

          {formik.errors.address ? (
            <div className="error">
              {formik.touched.address && formik.errors.address}
            </div>
          ) : null}
        </div>

        <div className="form_control">
          <label className="modal-form_label" htmlFor="number">
            Мобільний телефон
          </label>
          <PatternFormat
            className="modal-form_input"
            id="number"
            name="number"
            onChange={formik.handleChange}
            value={formik.values.number}
            onBlur={formik.handleBlur}
            format="(+###) ###-##-####"
            mask="#"
            placeholder="(+###) ###-##-####"
          />
          {formik.errors.number ? (
            <div className="error">
              {formik.touched.number && formik.errors.number}
            </div>
          ) : null}
        </div>

        <button className="modal-form_btn" type="submit">
          Checkout
        </button>
      </form>
    </div>
  );
}

export default ModalFormBuy;
