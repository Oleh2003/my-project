import React, { createContext, useState } from "react";

export const ViewModeContext = createContext();

export const ViewModeProvider = ({ children }) => {
  const [viewMode, setViewMode] = useState("card"); 

  const toggleViewMode = () => {
    setViewMode(prevMode => prevMode === "card" ? "table" : "card"); 
  };

  return (
    <ViewModeContext.Provider value={{ viewMode, toggleViewMode }}>
      {children}
    </ViewModeContext.Provider>
  );
};