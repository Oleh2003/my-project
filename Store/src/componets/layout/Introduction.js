import React from "react";
import previe from "../../image/previe.png";
import iconOne from "../../image/iconOne.svg";
import iconTwo from "../../image/iconTwo.svg";

function Introduction() {
  return (
    <div className="introduction">
      <div className="introduction_box box-intoduction">
        <div>
          <h3 className="box-intoduction_label">Exclusive Smart Watch</h3>
          <h1 className="box-intoduction_title">
            Let go of the Challenge See yourself better
          </h1>
          <div className="box-intoduction_text">
            <p>
              Connect your conversations with the tools and services that you
              use to get the job done. With over 1,500 apps and a robust API
            </p>
          </div>

          <button onClick={() => {}} className="button">
            Get started
          </button>

          <div className="icon">
            <div className="box-intoduction_icon ">
              <img className="icon_img" src={iconOne} alt="icon" />
              <div>
                <span className="icon_span">+12k</span>
                <h3 className="icon_title">Projects done</h3>
              </div>
            </div>
            <div className="box-intoduction_icon ">
              <img className="icon_img" src={iconTwo} alt="icon" />
              <div>
                <span className="icon_span">+68k</span>
                <h3 className="icon_title">Custommers</h3>
              </div>
            </div>
          </div>
        </div>
        <div className="box-intoduction_img">
          <img src={previe} alt="appleWatch" />
        </div>
      </div>
    </div>
  );
}

export default Introduction;
