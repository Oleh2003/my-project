import React, { useState } from "react";
import photo from "../../image/subcribe-photo.png";

function Subcribe() {
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState(false);

  const handleChange = (event) => {
    console.log(event.target.value);
    setEmail(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    localStorage.setItem("subcribe", email);
    setEmail("");
    setMessage(true);

    setTimeout(() => {
      setMessage(false);
    }, 2000);
  };
  return (
    <div className="subcribe">
      <div className="subcribe_block">
        <h2 className="subcribe_title">Subscribe our newsletter</h2>
        <div className="subcribe_text">
          <p>
            By clicking the button, you are agreeing with our Term & Conditions
          </p>
        </div>
        <form className="subcribe_form" onSubmit={handleSubmit}>
          <input
            type="email"
            className="subcribe_input"
            placeholder="Enter you email..."
            value={email}
            onChange={handleChange}
          />
          <button type="submit" className="subcribe_button">
            &#10132;
          </button>
        </form>
        {message && <div className="message">Email saved!</div>}
      </div>
      <div className="subcribe_images">
        <img src={photo} alt="subcribePhoto" />
      </div>
    </div>
  );
}

export default Subcribe;
