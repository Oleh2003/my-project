import React from "react";
import categories1 from "../../image/categories-1.svg"
import categories2 from "../../image/categories-2.svg"
import categories3 from "../../image/categories-3.svg"
import categories4 from "../../image/categories-4.svg"

function Categoties() {
  return (
    <div className="categories">
      <h2 className="main-title">Explore by Categories.</h2>
      <div className="categories_text">
        <p>
          We provide many categories, choose a category according to your
          expertise to make it easier to find a job.
        </p>
      </div>
      <div className="categories_content">
        <div className="categories_flex content">
          <h2 className="content_title">Popular Categories</h2>
          <div className="content_box">
            <p className="content_text">Computer - Laaptop</p>
            <span className="content_span">10</span>
          </div>
          <span className="line"></span>
          <div className="content_box">
            <p className="content_text">Smart phone & Tablets</p>
            <span className="content_span">12</span>
          </div>
          <span className="line"></span>
          <div className="content_box">
            <p className="content_text">Fashion & Accessories</p>
            <span className="content_span">14</span>
          </div>
          <span className="line"></span>
          <div className="content_box">
            <p className="content_text">Halth & Beauty</p>
            <span className="content_span">8</span>
          </div>
        </div>
        <div className="categories_box box">
    <div className="box_table">
      <span className="box_span"> <img src={categories1} alt="icon" /></span>
      <h3 className="box_title">Computer for Designer, Art</h3>
    </div>
    <div className="box_table">
      <span className="box_span"> <img src={categories2} alt="icon" /></span>
      <h3 className="box_title">Laptop for office, students</h3>
    </div>
    <div className="box_table">
      <span className="box_span"> <img src={categories3} alt="icon" /></span>
      <h3 className="box_title">Software, card, book</h3>
    </div>
    <div className="box_table">
      <span className="box_span"> <img src={categories1} alt="icon" /></span>
      <h3 className="box_title">Sport, Home, Outdoor</h3>
    </div>
    <div className="box_table">
      <span className="box_span"> <img src={categories4} alt="icon" /></span>
      <h3 className="box_title">Explore More</h3>
    </div>
        </div>
      </div>
    </div>
  );
}

export default Categoties;
