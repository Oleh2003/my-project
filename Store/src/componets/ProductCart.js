import React from "react";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import Button from "./Button";
import "../scss/ProductCard.scss";
import Modal from "./Modal";
import {
  setModal,
  setModalFrom,
} from "../redux/action";
import ModalFormBuy from "./ModalFormBuy";

const ProductCart = ({ card, deleteFromCart, toggleFavorite, isFavorite }) => {
  const isModalOpen = useSelector((state) => state.modalReducer.isModalOpen);
  const selectedItemId = useSelector(
    (state) => state.modalReducer.selectedItemId
  );

  const isModalFormOpen = useSelector(
    (state) => state.modalReducer.isModalFormOpen
  );
  const selectedItemIdFrom = useSelector(
    (state) => state.modalReducer.selectedItemIdFrom
  );

  const dispatch = useDispatch();

  const handleDeleteFromCart = () => {
    dispatch(setModal(true, card.id));
  };

  const handleModalClose = () => {
    dispatch(setModal(false, card.id));
    deleteFromCart(card);
  };

  const ModalClose = () => {
    dispatch(setModal(false, card.id));
  };

  const openModalFormInCart = () => {
    dispatch(setModalFrom(true, card.id));
  };

  const closeModalFromInCart = () => {
    dispatch(setModalFrom(false, card.id));
  };


  return (
    <li className="card-item">
      <div className="card-item_img">
        <img
          src={card.imagePath}
          alt={card.name}
          
        />
        <span onClick={handleDeleteFromCart} className="card-close">
          &times;
        </span>
      </div>
      <div className="card-item-content">
        <div className="card-item_box">
          <h2 className="card-item_title">{card.name}</h2>
          <span className="card-item_star" onClick={() => toggleFavorite(card)}>
            {isFavorite ? "★" : "☆"}
          </span>
        </div>
        <div className="card-item_flex">
        <p className="card-item_price"> {card.price} </p>
        <p className="card-item_color">Color: {card.color}</p></div>
      
      </div>
      <Button
        onClick={openModalFormInCart}
        text={"Buy All"}
        backgroundColor={"rgba(190, 225, 230, 1)"}
      />
      <ModalFormBuy
        isOpenForm={isModalFormOpen && selectedItemIdFrom === card.id}
        onRequestCloseFrom={closeModalFromInCart}
        closeButtonForm={true}
        card={card}
        
      />
      <Modal
        header="Confirmation"
        closeButton={true}
        text={`Product "${card.name}" has been delete from the cart.`}
        isOpen={isModalOpen && selectedItemId === card.id}
        onRequestClose={ModalClose}
        actions={
          <>
            <Button
              backgroundColor="rgb(47, 112, 233)"
              text="Ok"
              onClick={handleModalClose}
            />
            <Button
              backgroundColor="rgb(47, 112, 233)"
              text="Cancel"
              onClick={ModalClose}
            />
          </>
        }
      />
    </li>
  );
};

ProductCart.propTypes = {
  card: PropTypes.object.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool.isRequired,
};

export default ProductCart;
