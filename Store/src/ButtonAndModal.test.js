import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Modal from './componets/Modal';
import Button from './componets/Button';

describe('Modal ', () => {
  test('does not render modal when isOpen is false', () => {
    const onRequestClose = jest.fn();
    const { queryByText } = render(
      <Modal
        header="Confirmation "
        closeButton={true}
        text="Product  has been added to the cart"
        isOpen={false}
        onRequestClose={onRequestClose}
        actions={<Button text="OK" onClick={jest.fn()} />}
      />
    );

    expect(queryByText('Confirmation')).toBeNull();
    expect(queryByText('Product  has been added to the cart')).toBeNull();
  });
  test('calls onRequestClose when close button is clicked', () => {
    const onRequestClose = jest.fn();
    const { getByText } = render(
      <Modal
        header="Confirmation"
        closeButton={true}
        text="Product  has been added to the cart"
        isOpen={true}
        onRequestClose={onRequestClose}
        actions={<Button text="OK" onClick={jest.fn()} />}
      />
    );

    fireEvent.click(getByText('×'));
    expect(onRequestClose).toHaveBeenCalledTimes(1);
  });
});

describe('Button Component', () => {
  test('calls onClick when button is clicked', () => {
    const onClick = jest.fn();
    const { getByText } = render(
      <Button text="OK" onClick={onClick} />
    );

    fireEvent.click(getByText('OK'));
    expect(onClick).toHaveBeenCalledTimes(1);
  });

  test('calls onClick when button is clicked', () => {
    const onClick = jest.fn();
    const { getByText } = render(
      <Button text="CLOSE" onClick={onClick} />
    );
    fireEvent.click(getByText('CLOSE'));
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
