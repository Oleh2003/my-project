import {
  ADD_CART,
  DELETE_CART,
  DELETE_FROM_LOCAL_STORAGE_CART,
  LOAD_CART,
  SAVE_CART,
  SET_MODAL_FORM_DELETE_CART,
} from "./type";

const savedCart = localStorage.getItem("cart");
const initialState = savedCart ? JSON.parse(savedCart) : [];

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_CART:
      return [...state];

    case ADD_CART:
      return [...state, action.data];

    case SAVE_CART:
      return [...action.data];

    case DELETE_CART:
      return state.filter((card) => card.id !== action.data.id);

    case SET_MODAL_FORM_DELETE_CART:
      return state.filter((card) => card.id !== action.data.id);

      case DELETE_FROM_LOCAL_STORAGE_CART:
        return []

    default:
      return state;
  }
};
