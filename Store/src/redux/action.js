import { ADD_CART, 
    ADD_FAVORITES, 
    DELETE_CART, 
    DELETE_FAVORITES, 
    DELETE_FROM_LOCAL_STORAGE_CART, 
    LOAD_CART, 
    LOAD_FAVORITES, 
    SAVE_CART, 
    SAVE_FAVORITES, 
    SET_DATA, 
    SET_MODAL, 
    SET_MODAL_FORM,
    SET_MODAL_FORM_DELETE_CART} from "./type";

export function setData(data){
   return {
    type: SET_DATA,
    data,
   } 
} 

export function addCart(data){
    return {
        type: ADD_CART,
        data,
    }
}

export function deleteCart(data){
    return {
        type: DELETE_CART,
        data,
    }
}

export function addFavorites(product){
    return {
        type: ADD_FAVORITES,
        data: product
    }
}
export function deleteFavorites(product){
    return {
        type: DELETE_FAVORITES,
        data: product
    }
}

export function loadCart(savedCart){
    return {
        type: LOAD_CART,
        data: JSON.parse(savedCart)
    }
}

export function loadFavorites(savedFavorites){
    return {
        type: LOAD_FAVORITES,
        data: JSON.parse(savedFavorites)
    }
}

export function saveCart(cart){
    localStorage.setItem('cart', JSON.stringify(cart));
    return {
        type: SAVE_CART,
        data: cart
    }
}

export function saveFavorites(favorites){
    localStorage.setItem('favorites', JSON.stringify(favorites));
    return {
        type: SAVE_FAVORITES,
        data: favorites
    }
}

export function setModal(isOpen, id){
    return {
        type: SET_MODAL,
        data: {isOpen, id}
    }
}

export function setModalFrom(isOpenFrom, id){
    return {
        type: SET_MODAL_FORM,
        data: {isOpenFrom, id}
    }
}

export function setModalFormDeleteCart(data){
    return {
        type: SET_MODAL_FORM_DELETE_CART,
        data,
    }
}

export function deleteFromLocalStorageCart(cart){
    localStorage.removeItem("cart"); 
    return {
        type: DELETE_FROM_LOCAL_STORAGE_CART,
        data: cart
    }
}