'use strict'

document.addEventListener('click', menuOpen);

function menuOpen(event) {
    let target = event.target;
    if(target.closest('.icon-menu')){
        document.documentElement.classList.toggle('menu-open');
    }
    
}