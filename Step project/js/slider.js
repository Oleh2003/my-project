'use string'

const aboutHidden = document.querySelectorAll(".about-hidden");
const aboutCarouselImg = document.querySelectorAll(".about-carousel-item-img");
const previousBtn = document.querySelector(".previous-btn");
const nextBtn = document.querySelector(".next-btn");

const removeActiveClassInAboutHidden = () => {
    aboutHidden.forEach(e => {
        e.classList.remove('about-active');
    });
};

const removeActiveClassInAboutCarouselImg = () => {
    aboutCarouselImg.forEach(e => {
        e.classList.remove('about-carousel-active');
    });
};

aboutCarouselImg.forEach(element => {
    element.addEventListener('click', (event) => {
        removeActiveClassInAboutCarouselImg();
        element.classList.add('about-carousel-active');
        aboutHidden.forEach(item => {
            item.classList.remove('about-active');
            if (item.dataset.user === event.target.dataset.user) {
                item.classList.add('about-active');
            };
        });
    });
});
let currentImg = 2;

const changeImage = () => {
    removeActiveClassInAboutHidden();
    removeActiveClassInAboutCarouselImg();
    aboutHidden[currentImg].classList.add('about-active');
    aboutCarouselImg[currentImg].classList.add('about-carousel-active');
};

previousBtn.addEventListener("click", () => {
    if (currentImg < 1) {
        currentImg = aboutHidden.length;
    }
    currentImg--;
    changeImage();
})

nextBtn.addEventListener("click", () => {
    currentImg++;
    if (currentImg === aboutHidden.length) {
        currentImg = 0;
    }
    changeImage();
})
