"use strict"

window.addEventListener('load', tab)

function tab() {
    document.querySelector('.tabs').addEventListener('click', fTabs)

    function fTabs(event) {
        if (event.target.className == 'tabs-nav-item') {
            let dataTab = event.target.getAttribute('data-tab-name');

            let tabs = document.querySelectorAll('.tabs-nav-item')
            for( let i = 0; i < tabs.length; i++){
                tabs[i].classList.remove('is-active')
            }
            event.target.classList.add('is-active')

            let tabContent = document.querySelectorAll('.tab');

            for (let i = 0; i < tabContent.length; i++) {
                if (dataTab == i) {
                    tabContent[i].classList.add('is-active')
                } else {
                    tabContent[i].classList.remove('is-active');
                }
            }
        }
    }

}


