'use strict'

window.addEventListener('load', AmazingWorks)

function AmazingWorks(){
    document.addEventListener('click' , documentActions)
}

function documentActions(e){
    let targetElement = e.target;

    if(targetElement.classList.contains('amazing_work-list') && !targetElement.classList.contains('active')){

        const activeElement = document.querySelector('.amazing_work-list.active');
        const elements = document.querySelectorAll('.image-box-amazing_work');
        const elementType = targetElement.dataset.workType;


        activeElement.classList.remove('active');
        targetElement.classList.add('active');


        elements.forEach(element =>{
            !elementType || element.dataset.workType === elementType ?
            element.hidden = false : element.hidden = true;
        });
    }
}


const btnLoad = document.querySelector('.load_more-btn ');

btnLoad.addEventListener('click', viewDiv);

function viewDiv(){

    let gridBox = document.querySelector(".grid-box-amazing_work-load_more")
    if(gridBox.style.display = "grid"){
        document.querySelector(".load_more-btn").style.display = "none";
    };
   
  };